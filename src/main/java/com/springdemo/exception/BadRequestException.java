/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:17:28 PM BadRequestException.java
 *         com.springdemo.exception SpringDemo
 */
package com.springdemo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:17:28 PM
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {
  public BadRequestException(String message) {
    super(message);
  }

  public BadRequestException(String message, Throwable cause) {
    super(message, cause);
  }
}
