/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:16:39 PM AppException.java com.springdemo.exception
 *         SpringDemo
 */
package com.springdemo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:16:39 PM
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AppException extends RuntimeException {
  /**
   * 
   */
  
  private static final long serialVersionUID = 6162251410475383738L;
  
  private int code;
  private String type;
  private String message;
  /**
   * @author Nil Panchal
   * Jun 7, 2022
   * 1:20:33 PM
   * @param code
   * @param type
   * @param message
   */
  public AppException(int code, String type, String message) {
    this.code = code;
    this.type = type;
    this.message = message;
  }

  /**
   * @author Nil Panchal
   * Jun 7, 2022
   * 1:20:31 PM
   */
  public AppException() {
    super();
  }

  /**
   * @author Nil Panchal
   * Jun 7, 2022
   * 1:20:31 PM
   * @param cause
   */
  public AppException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

  public AppException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public AppException(String message) {
    super(message);
    this.code = HttpStatus.BAD_REQUEST.value();
    this.type = "fail";
  }

  public AppException(String message, Throwable cause) {
    super(message, cause);
  }

  public static AppException invalidRequestException() {
    return new AppException(ExceptionConstants.INVALID_REQUEST_TYPE_ERROR);
  }

  public int getCode() {
    return code;
  }

  public String getType() {
    return type;
  }

  public String getMessage() {
    return message;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setMessage(String message) {
    this.message = message;
  }


}
