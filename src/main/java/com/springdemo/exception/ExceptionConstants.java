/**
 * @author Nil Panchal
 * Mex IT
 * Jun 6, 2022
 * 8:26:17 PM
 * ExceptionConstants.java
 * com.springdemo.exception
 * SpringDemo
 */
package com.springdemo.exception;

/**
 * @author Nil Panchal
 * Mex IT
 * Jun 6, 2022
 * 8:26:17 PM
 */
public class ExceptionConstants {

  public static final String INVALID_REQUEST_TYPE_ERROR = "INVALID_REQUEST";
  
  public static final int INVALID_USER_DETAILS_ERROR_CODE = 4001;
  public static final String INVALID_USER_DETAILS_ERROR_TEXT = "Username or password is incorrect";
  
  public static final int EMAIL_ALREADY_USED_ERROR_CODE = 4002;
  public static final String EMAIL_ALREADY_USED_ERROR_TEXT = "Email Address already in use!";
  
  public static final int INVALID_POST_ID_ERROR_CODE = 4003;
  public static final String INVALID_POST_ID_ERROR_TEXT = "Invalid post id";
}
