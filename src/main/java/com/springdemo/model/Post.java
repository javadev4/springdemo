/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:02:48 PM Post.java com.springdemo.model SpringDemo
 */
package com.springdemo.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import com.springdemo.constant.Constants;
import com.springdemo.utils.Utils;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:02:48 PM
 */

@Entity(name = "post")
@Table(name = "post")
public class Post extends BaseEntity {

  /**
   * 
   */
  private static final long serialVersionUID = 1870860518644554665L;
  @NotBlank
  private String text;

  @ManyToOne(fetch = FetchType.LAZY)
  @OnDelete(action = OnDeleteAction.CASCADE)
  @JoinColumn(name = "created_by")
  private User createdBy;


  

  /**
   * @author Nil Panchal
   * Jun 7, 2022
   * 10:37:03 AM
   * @param text
   * @param createdBy
   */
  public Post(String text, User createdBy) {
    this.text = text;
    this.createdBy = createdBy;
  }

  /**
   * @author Nil Panchal Jun 6, 2022 10:06:04 PM
   * @param text
   */
  public Post(@NotBlank String text) {
    this.text = text;
  }

  /**
   * @author Nil Panchal Jun 6, 2022 10:06:02 PM
   */
  public Post() {
    super();
  }

  @Override
  public String generatShortId() {
    return Utils.getShortId(Constants.POST_INTIAL);
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public User getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(User createdBy) {
    this.createdBy = createdBy;
  }



}
