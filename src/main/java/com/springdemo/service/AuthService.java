/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:14:07 PM AuthService.java com.springdemo.service
 *         SpringDemo
 */
package com.springdemo.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.springdemo.config.UserPrincipal;
import com.springdemo.constant.Constants;
import com.springdemo.constant.Messages;
import com.springdemo.exception.ExceptionConstants;
import com.springdemo.model.User;
import com.springdemo.payload.request.LoginRequest;
import com.springdemo.payload.request.SignUpRequest;
import com.springdemo.payload.response.BaseResponse;
import com.springdemo.payload.response.LoginResponse;
import com.springdemo.payload.response.PersonalDetailsResponse;
import com.springdemo.repository.UserRepository;
import com.springdemo.security.JwtTokenProvider;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:14:07 PM
 */
@Service
public class AuthService {

  @Autowired
  AuthenticationManager authenticationManager;
  @Autowired
  UserRepository userRepository;
  @Autowired
  PasswordEncoder passwordEncoder;
  @Autowired
  JwtTokenProvider tokenProvider;

  public BaseResponse<LoginResponse> login(LoginRequest loginRequest) {
    Authentication authentication =
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
            loginRequest.getEmail(), loginRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    UserPrincipal userDetails = (UserPrincipal) authentication.getPrincipal();
    String jwt = tokenProvider.generateToken(authentication);
    User user = userDetails.getCurrentUser();
    user.setToken(jwt);
    userRepository.saveAndFlush(user);
    return BaseResponse.success(new LoginResponse(jwt, userDetails));
  }


  public BaseResponse<PersonalDetailsResponse> createUser(SignUpRequest signUpRequest) {
    Optional<User> userDetails = userRepository.findByEmail(signUpRequest.getEmail());

    if (userDetails.isPresent()) {
      return new BaseResponse<PersonalDetailsResponse>(HttpStatus.BAD_REQUEST.value(), false,
          ExceptionConstants.INVALID_REQUEST_TYPE_ERROR,
          ExceptionConstants.EMAIL_ALREADY_USED_ERROR_TEXT);
    }

    // Creating user's account
    User user = new User(signUpRequest.getFirstName(), signUpRequest.getLastName(),
        signUpRequest.getEmail(), signUpRequest.getPassword());
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    userRepository.save(user);


    return BaseResponse.success(new PersonalDetailsResponse(user), Messages.USER_CREATED);

  }
}
