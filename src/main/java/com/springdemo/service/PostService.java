/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:59:35 PM PostService.java com.springdemo.service
 *         SpringDemo
 */
package com.springdemo.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.springdemo.config.UserPrincipal;
import com.springdemo.constant.Constants;
import com.springdemo.constant.Messages;
import com.springdemo.exception.ExceptionConstants;
import com.springdemo.model.Post;
import com.springdemo.payload.request.PostRequest;
import com.springdemo.payload.response.ApiResponse;
import com.springdemo.payload.response.BaseResponse;
import com.springdemo.payload.response.PostResponse;
import com.springdemo.repository.PostRepository;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:59:35 PM
 */
@Service
public class PostService {
  private static final Logger logger = LoggerFactory.getLogger(PostService.class);

  @Autowired
  PostRepository postRepository;

  public BaseResponse<PostResponse> create(UserPrincipal userPrincipal, PostRequest request) {
    Post post = new Post(request.getText(), userPrincipal.getCurrentUser());
    postRepository.saveAndFlush(post);
    return BaseResponse.success(new PostResponse(post), Messages.POST_CREATED);

  }
  
  public BaseResponse<List<PostResponse>> getAllPosts(UserPrincipal userPrincipal) {
    List<PostResponse> userPosts = postRepository.findByCreatedBy(userPrincipal.getCurrentUser())
        .stream().map(post -> new PostResponse(post)).collect(Collectors.toList());
    return BaseResponse.success(userPosts);
  }

  public BaseResponse<PostResponse> getSinglePost(UserPrincipal userPrincipal, String postId) {
    logger.info("postId : " + postId);

    Optional<Post> userPost = postRepository.findByShortId(postId);

    if (!userPost.isPresent()) {
      return new BaseResponse<PostResponse>(HttpStatus.BAD_REQUEST.value(), false, Constants.FAIL,
          ExceptionConstants.INVALID_POST_ID_ERROR_TEXT);
    }
    return BaseResponse.success(new PostResponse(userPost.get()));
  }

  public BaseResponse<PostResponse> updateSinglePost(UserPrincipal userPrincipal, String postId,
      PostRequest request) {
    logger.info("postId : " + postId);
    Optional<Post> userPost = postRepository.findByShortId(postId);
    if (!userPost.isPresent()) {
      return new BaseResponse<PostResponse>(HttpStatus.BAD_REQUEST.value(), false, Constants.FAIL,
          ExceptionConstants.INVALID_POST_ID_ERROR_TEXT);
    }
    Post post = userPost.get();
    post.setText(request.getText());
    postRepository.saveAndFlush(post);
    return BaseResponse.success(new PostResponse(post));
  }

  

  public ApiResponse deleteSinglePost(UserPrincipal userPrincipal, String postId) {
    logger.info("postId : " + postId);
    Optional<Post> userPost = postRepository.findByShortId(postId);
    if (!userPost.isPresent()) {
      return new ApiResponse(HttpStatus.BAD_REQUEST.value(), Constants.FAIL,
          ExceptionConstants.INVALID_POST_ID_ERROR_TEXT);
    }
    postRepository.delete(userPost.get());
    return new ApiResponse(HttpStatus.OK.value(), Constants.SUCCESS, Messages.POST_DELETED);
  }

}
