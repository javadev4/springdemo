/**
 * @author Nil Panchal Mex IT May 29, 2022 7:01:29 PM JwtAuthenticationEntryPoint.java
 *         com.springdemo.component SpringDemo
 */
package com.springdemo.component;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.springdemo.constant.Constants;
import com.springdemo.exception.ExceptionConstants;
import com.springdemo.payload.response.ApiResponse;

/**
 * @author Nil Panchal Mex IT May 29, 2022 7:01:29 PM
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

  @Autowired
  private ObjectMapper objectMapper;

  @Override
  public void commence(HttpServletRequest request, HttpServletResponse response,
      AuthenticationException authException) throws IOException, ServletException {
    String message = authException.getMessage();
    ApiResponse apiResponse = new ApiResponse();
    apiResponse.setStatus(Constants.FAIL);
    apiResponse.setMessage(authException.getMessage());
    response.addHeader(HttpHeaders.CONTENT_TYPE, Constants.UTF8_JSON_CONTENT_TYPE);
    // set the response status code
    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    apiResponse.setStatus(Constants.FAIL);
    apiResponse.setCode(ExceptionConstants.INVALID_USER_DETAILS_ERROR_CODE);
    apiResponse.setMessage(ExceptionConstants.INVALID_USER_DETAILS_ERROR_TEXT);
    apiResponse.setType(ExceptionConstants.INVALID_REQUEST_TYPE_ERROR);

    System.out.println(authException.getMessage());
    authException.printStackTrace();
    
    if (message == null || message.equalsIgnoreCase("Bad credentials")) {
      apiResponse.setCode(HttpStatus.BAD_REQUEST.value());
      apiResponse.setMessage("Invalid user credentials");
    } else if (message
        .equalsIgnoreCase("Full authentication is required to access this resource")) {
      apiResponse.setCode(HttpStatus.BAD_REQUEST.value());
      apiResponse.setMessage("Invalid Token");

    } else {
      apiResponse.setCode(HttpStatus.BAD_REQUEST.value());
      apiResponse.setMessage("Invalid Request");
    }


    objectMapper.writeValue(response.getOutputStream(), apiResponse);
    // commit the response
    response.flushBuffer();
  }
}
