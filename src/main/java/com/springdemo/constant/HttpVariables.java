/**
 * @author Nil Panchal
 * Mex IT
 * Jun 7, 2022
 * 11:20:06 AM
 * HttpVariables.java
 * com.springdemo.controller
 * SpringDemo
 */
package com.springdemo.constant;

/**
 * @author Nil Panchal
 * Mex IT
 * Jun 7, 2022
 * 11:20:06 AM
 */
public class HttpVariables {
  public static final String POST_ID = "postId";
}
