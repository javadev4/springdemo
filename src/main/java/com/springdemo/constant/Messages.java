/**
 * @author Nil Panchal Mex IT Jun 6, 2022 11:08:59 PM Messages.java com.springdemo.constant
 *         SpringDemo
 */
package com.springdemo.constant;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 11:08:59 PM
 */
public class Messages {
  
  public static final String USER_CREATED = "User created successfuly";
  public static final String POST_CREATED = "Post created successfuly";
  public static final String POST_UPDATED = "Post udpated successfuly";
  public static final String POST_DELETED = "Post deleted successfuly";
}
