/**
 * @author Nil Panchal Mex IT May 28, 2022 10:32:33 PM Constants.java com.springdemo.constant
 *         SpringDemo
 */
package com.springdemo.constant;

/**
 * @author Nil Panchal Mex IT May 28, 2022 10:32:33 PM
 */
public final class Constants {

  public static final String UUID_GENERATOR = "UUID";
  public static final String UUID_GENERATOR_STRATEGY = "org.hibernate.id.UUIDGenerator";
  public static final String UTF8_JSON_CONTENT_TYPE = "application/json;charset=UTF-8";

  public static final int SHORT_ID_POSTFIX = 5;

  public static final String SUCCESS = "success";
  public static final String FAIL = "fail";

  public static final String OK = "Ok";

  public static final String USER_INTIAL = "usr";
  public static final String POST_INTIAL = "pst";

  public static final String TOKEN_PREFIX = "Bearer ";

  public static final String LONG_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
  public static final String EUROPE_BERLIN_ZONE = "Europe/Berlin";
  
  
}
