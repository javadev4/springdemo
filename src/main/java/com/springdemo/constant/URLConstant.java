/**
 * @author Nil Panchal
 * Mex IT
 * Jun 6, 2022
 * 10:09:28 PM
 * URLConstant.java
 * com.springdemo.constant
 * SpringDemo
 */
package com.springdemo.constant;

/**
 * @author Nil Panchal
 * Mex IT
 * Jun 6, 2022
 * 10:09:28 PM
 */
public class URLConstant {
  public static final String AUTH = "/api/auth";
  public static final String AUTH_SIGN_IN = "/signin";
  public static final String AUTH_SIGN_UP = "/signup";
  
  
  
  public static final String POST = "/post";
  public static final String SINGLE_POST = "/post/{postId}";
}
