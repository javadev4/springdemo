/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:06:54 PM PostRepository.java com.springdemo.repository
 *         SpringDemo
 */
package com.springdemo.repository;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.springdemo.model.Post;
import com.springdemo.model.User;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:06:54 PM
 */
@Repository
@Transactional
public interface PostRepository extends BaseRepository<Post> {
  Optional<Post> findByShortId(String postId);
  List<Post> findByCreatedBy(User createdBy);
}
