/**
 * @author Nil Panchal Mex IT Jun 6, 2022 3:18:57 PM CustomUserDetailsService.java
 *         com.springdemo.security SpringDemo
 */
package com.springdemo.security;


import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.springdemo.config.UserPrincipal;
import com.springdemo.model.User;
import com.springdemo.repository.UserRepository;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 3:18:57 PM
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

  private static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);
  @Autowired
  UserRepository userRepository;

  private User user;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    logger.info("username  : " + username);


    Optional<User> userDetails = userRepository.findByEmail(username);
    logger.info("username isPresent : " + userDetails.isPresent());
    if (userDetails.isPresent()) {
      this.user = userDetails.get();
      return UserPrincipal.create(userDetails.get());
    } else {
      this.user = null;
    }
    return null;
  }

  public User getUser() {
    return this.user;
  }


  @Transactional
  public UserDetails loadUserByIdAndToken(String id, String token) {
    logger.info("loadUserByIdAndToken :  " + id);
    logger.info("loadUserByIdAndToken token :  " + token);
    try {
      /**
       * Find user details by short id and token Token is stored in DB when user logged in
       */
      Optional<User> userDetails = userRepository.findByShortIdAndToken(id, token);
      if (userDetails.isPresent()) {
        this.user = userDetails.get();
      }
      return UserPrincipal.create(this.user);
    } catch (EmptyResultDataAccessException e) {
      logger.error("User id not found");
      return null;
    }
  }


}
