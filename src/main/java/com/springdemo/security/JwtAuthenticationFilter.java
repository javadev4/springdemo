/**
 * @author Nil Panchal Mex IT Jun 6, 2022 3:52:07 PM JwtAuthenticationFilter.java
 *         com.springdemo.security SpringDemo
 */
package com.springdemo.security;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.springdemo.config.UserPrincipal;
import com.springdemo.constant.Constants;
import com.springdemo.payload.response.ApiResponse;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 3:52:07 PM
 */
public class JwtAuthenticationFilter extends OncePerRequestFilter {
  private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

  @Autowired
  private JwtTokenProvider tokenProvider;

  @Autowired
  private CustomUserDetailsService customUserDetailsService;

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {

    logger.info("doFilterInternal");
    try {
      String jwt = getJwtFromRequest(request);
      if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
        String userId = tokenProvider.getUserIdFromJWT(jwt);
        System.out.println("User id : " + userId);

        UserPrincipal userDetails =
            (UserPrincipal) customUserDetailsService.loadUserByIdAndToken(userId, jwt);
        userDetails.setToken(jwt);

        UsernamePasswordAuthenticationToken authentication =
            new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());


        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authentication);
      }


      filterChain.doFilter(request, response);
    } catch (ExpiredJwtException exception) {
      customException(exception, response, "Token is expired");
    } catch (IllegalArgumentException exception) {
      customException(exception, response, "Token not found");
    } catch (UnsupportedJwtException | MalformedJwtException exception) {
      customException(exception, response, "Invalid Token");
    }
  }


  private <T> void customException(T exception, HttpServletResponse httpServletResponse,
      String message) throws JsonProcessingException, IOException {
    ApiResponse apiResponse =
        new ApiResponse(HttpStatus.BAD_REQUEST.value(), Constants.FAIL, message);
    httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
    httpServletResponse.getWriter().write(new ObjectMapper().writeValueAsString(apiResponse));
  }


  /**
   * To get JWT token from the request
   *
   * @param httpServletRequest
   * @return String
   */
  private String getJwtFromRequest(HttpServletRequest httpServletRequest) {
    String requestURI = httpServletRequest.getRequestURI();
    String bearerToken = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
    if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(Constants.TOKEN_PREFIX)) {
      return bearerToken.substring(Constants.TOKEN_PREFIX.length(), bearerToken.length());
    }
    return null;
  }

}
