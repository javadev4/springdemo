/**
 * @author Nil Panchal
 * Mex IT
 * Jun 6, 2022
 * 4:00:47 PM
 * CurrentUser.java
 * com.springdemo.security
 * SpringDemo
 */
package com.springdemo.security;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.security.core.annotation.AuthenticationPrincipal;

/**
 * @author Nil Panchal
 * Mex IT
 * Jun 6, 2022
 * 4:00:47 PM
 */
@Target({ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@AuthenticationPrincipal
public @interface CurrentUser {

}
