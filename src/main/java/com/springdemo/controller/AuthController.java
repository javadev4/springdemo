/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:19:38 PM AuthController.java com.springdemo.controller
 *         SpringDemo
 */
package com.springdemo.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.springdemo.constant.URLConstant;
import com.springdemo.payload.request.LoginRequest;
import com.springdemo.payload.request.SignUpRequest;
import com.springdemo.payload.response.BaseResponse;
import com.springdemo.payload.response.LoginResponse;
import com.springdemo.payload.response.PersonalDetailsResponse;
import com.springdemo.service.AuthService;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:19:38 PM
 */
@RestController
@RequestMapping(URLConstant.AUTH)
public class AuthController {

  @Autowired
  AuthService authService;

  @PostMapping(URLConstant.AUTH_SIGN_IN)
  public ResponseEntity<BaseResponse<LoginResponse>> authenticateUser(
      @Valid @RequestBody LoginRequest loginRequest) {
    BaseResponse<LoginResponse> response = authService.login(loginRequest);
    return ResponseEntity.status(response.getCode()).body(response);
  }

  @PostMapping(URLConstant.AUTH_SIGN_UP)
  public ResponseEntity<BaseResponse<PersonalDetailsResponse>> registerUser(
      @Valid @RequestBody SignUpRequest signUpRequest) {
    BaseResponse<PersonalDetailsResponse> response = authService.createUser(signUpRequest);
    return ResponseEntity.status(response.getCode()).body(response);
  }
}
