/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:08:26 PM PostController.java com.springdemo.controller
 *         SpringDemo
 */
package com.springdemo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.springdemo.config.UserPrincipal;
import com.springdemo.constant.HttpVariables;
import com.springdemo.constant.URLConstant;
import com.springdemo.payload.request.PostRequest;
import com.springdemo.payload.response.ApiResponse;
import com.springdemo.payload.response.BaseResponse;
import com.springdemo.payload.response.PostResponse;
import com.springdemo.security.CurrentUser;
import com.springdemo.service.PostService;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:08:26 PM
 */
@RestController
public class PostController {

  @Autowired
  PostService postService;

  @PostMapping(URLConstant.POST)
  public ResponseEntity<BaseResponse<PostResponse>> createPost(
      @CurrentUser UserPrincipal currentAdmin, @RequestBody PostRequest request) {
    BaseResponse<PostResponse> response = postService.create(currentAdmin, request);
    return ResponseEntity.status(response.getCode()).body(response);
  }

  @GetMapping(URLConstant.POST)
  public ResponseEntity<BaseResponse<List<PostResponse>>> getPosts(
      @CurrentUser UserPrincipal currentAdmin) {
    BaseResponse<List<PostResponse>> response = postService.getAllPosts(currentAdmin);
    return ResponseEntity.status(response.getCode()).body(response);
  }

  @GetMapping(URLConstant.SINGLE_POST)
  public ResponseEntity<BaseResponse<PostResponse>> getSiglePost(
      @CurrentUser UserPrincipal currentAdmin, @PathVariable(HttpVariables.POST_ID) String postId) {
    BaseResponse<PostResponse> response = postService.getSinglePost(currentAdmin, postId);
    return ResponseEntity.status(response.getCode()).body(response);
  }

  @PatchMapping(URLConstant.SINGLE_POST)
  public ResponseEntity<BaseResponse<PostResponse>> updatePost(
      @CurrentUser UserPrincipal currentAdmin, @PathVariable(HttpVariables.POST_ID) String postId,
      @RequestBody PostRequest request) {
    BaseResponse<PostResponse> response =
        postService.updateSinglePost(currentAdmin, postId, request);
    return ResponseEntity.status(response.getCode()).body(response);
  }


  @DeleteMapping(URLConstant.SINGLE_POST)
  public ResponseEntity<ApiResponse> updatePost(@CurrentUser UserPrincipal currentAdmin,
      @PathVariable(HttpVariables.POST_ID) String postId) {
    ApiResponse response = postService.deleteSinglePost(currentAdmin, postId);
    return ResponseEntity.status(response.getCode()).body(response);
  }


}
