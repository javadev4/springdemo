/**
 * @author Nil Panchal Mex IT Jun 6, 2022 11:04:10 PM PostResponse.java
 *         com.springdemo.payload.response SpringDemo
 */
package com.springdemo.payload.response;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import com.springdemo.constant.Constants;
import com.springdemo.model.Post;
import com.springdemo.payload.request.PostRequest;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 11:04:10 PM
 */
public class PostResponse extends PostRequest {

  private String id;
  private String text;
  private String createdBy;
  private String createdAt;

  public String getCreatedBy() {
    return createdBy;
  }

  public String getCreatedAt() {
    return createdAt;
  }



  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  /**
   * @author Nil Panchal Jun 6, 2022 11:05:12 PM
   * @param text
   * @param createdBy
   * @param createdAt
   */
  public PostResponse(String text, String createdBy, String createdAt) {
    super(text);
    this.createdBy = createdBy;
    this.createdAt = createdAt;
  }

  public PostResponse(Post post) {
    this.id = post.getShortId();
    this.text = post.getText();
    this.createdBy = post.getCreatedBy().getFirstName() + " " + post.getCreatedBy().getLastName();
    this.createdAt = DateTimeFormatter.ofPattern(Constants.LONG_DATE_TIME_FORMAT)
        .withZone(ZoneId.of(Constants.EUROPE_BERLIN_ZONE)).format(post.getCreatedAt());
  }

  /**
   * @author Nil Panchal Jun 6, 2022 11:05:14 PM
   */
  public PostResponse() {
    super();
  }

  /**
   * @author Nil Panchal Jun 6, 2022 11:05:14 PM
   * @param text
   */
  public PostResponse(String text) {
    super(text);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }


}
