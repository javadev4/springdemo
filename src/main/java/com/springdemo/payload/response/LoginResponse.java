/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:14:24 PM LoginResponse.java com.springdemo.payload
 *         SpringDemo
 */
package com.springdemo.payload.response;

import com.springdemo.config.UserPrincipal;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:14:24 PM
 */
public class LoginResponse extends PersonalDetailsResponse {
  private String accessToken;
  private String tokenType = "Bearer";

  /**
   * @author Nil Panchal Jun 6, 2022 10:24:23 PM
   * @param accessToken
   * @param tokenType
   */
  public LoginResponse(String accessToken, UserPrincipal user) {
    this.accessToken = accessToken;
    super.setEmail(user.getEmail());
    super.setFirstName(user.getFirstName());
    super.setLastName(user.getLastName());
  }

  /**
   * @author Nil Panchal Jun 6, 2022 10:24:21 PM
   */
  public LoginResponse() {
    super();
    // TODO Auto-generated constructor stub
  }

  public LoginResponse(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getTokenType() {
    return tokenType;
  }

  public void setTokenType(String tokenType) {
    this.tokenType = tokenType;
  }

}
