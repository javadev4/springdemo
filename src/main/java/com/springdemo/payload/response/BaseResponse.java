/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:18:15 PM BaseResponse.java com.springdemo.response
 *         SpringDemo
 */
package com.springdemo.payload.response;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.springdemo.constant.Constants;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:18:15 PM
 */
public class BaseResponse<T> {

  private int code;
  private boolean hasResult;
  private String status;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String message;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Map<String, String> links;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @Autowired
  private T data;

  /**
   * @author Nil Panchal Jun 6, 2022 10:19:01 PM
   */
  public BaseResponse() {
    super();
  }

  public BaseResponse(int code, boolean hasResult, String status, String message) {
    this.code = code;
    this.hasResult = false;
    this.status = status;
    this.message = message;
  }

  /**
   * @author Nil Panchal Jun 6, 2022 10:19:03 PM
   * @param code
   * @param hasResult
   * @param status
   * @param message
   * @param links
   * @param data
   */
  public BaseResponse(int code, boolean hasResult, String status, String message,
      Map<String, String> links, T data) {
    this.code = code;
    this.hasResult = hasResult;
    this.status = status;
    this.message = message;
    this.links = links;
    this.data = data;
  }

  public int getCode() {
    return code;
  }

  public boolean isHasResult() {
    return hasResult;
  }

  public String getStatus() {
    return status;
  }

  public String getMessage() {
    return message;
  }

  public Map<String, String> getLinks() {
    return links;
  }

  public T getData() {
    return data;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public void setHasResult(boolean hasResult) {
    this.hasResult = hasResult;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public void setLinks(Map<String, String> links) {
    this.links = links;
  }

  public void setData(T data) {
    this.data = data;
  }

  public static <K> BaseResponse<K> success(K data, String message) {
    BaseResponse<K> response = success();
    response.setData(data);
    response.setMessage(message);
    return response;
  }
  
  public static <K> BaseResponse<K> success(K data) {
    BaseResponse<K> response = success();
    response.setData(data);
    return response;
  }

  
  
  public static <K> BaseResponse<K> success() {
    BaseResponse<K> response = new BaseResponse<K>();
    response.setCode(HttpStatus.OK.value());
    response.setStatus(Constants.SUCCESS);
    response.setHasResult(true);
    return response;
  }
}
