/**
 * @author Nil Panchal Mex IT May 29, 2022 7:04:02 PM ApiResponse.java com.springdemo.response
 *         SpringDemo
 */
package com.springdemo.payload.response;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Nil Panchal Mex IT May 29, 2022 7:04:02 PM
 */
public class ApiResponse {

  private int code;
  private String status;
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String type;
  private String message;

  /**
   * @author Nil Panchal May 29, 2022 7:05:11 PM
   */
  public ApiResponse() {
    super();
  }

  /**
   * @author Nil Panchal May 29, 2022 7:05:13 PM
   * @param code
   * @param status
   * @param type
   * @param message
   */
  public ApiResponse(int code, String status, String type, String message) {
    this.code = code;
    this.status = status;
    this.type = type;
    this.message = message;
  }
  public ApiResponse(int code, String status, String message) {
    this.code = code;
    this.status = status;
    this.type = null;
    this.message = message;
  }

  public int getCode() {
    return code;
  }

  public String getStatus() {
    return status;
  }

  public String getType() {
    return type;
  }

  public String getMessage() {
    return message;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setMessage(String message) {
    this.message = message;
  }



}
