/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:16:40 PM PersonalDetailsResponse.java
 *         com.springdemo.response SpringDemo
 */
package com.springdemo.payload.response;

import com.springdemo.model.User;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:16:40 PM
 */
public class PersonalDetailsResponse {
  private String id;
  private String email;
  private String firstName;
  private String lastName;

  public PersonalDetailsResponse(User user) {
    this.id = user.getShortId();
    this.email = user.getEmail();
    this.firstName = user.getFirstName();
    this.lastName = user.getLastName();
  }

  /**
   * @author Nil Panchal Jun 7, 2022 11:41:53 AM
   * @param id
   * @param email
   * @param firstName
   * @param lastName
   */
  public PersonalDetailsResponse(String id, String email, String firstName, String lastName) {
    this.id = id;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  /**
   * @author Nil Panchal Jun 7, 2022 11:41:52 AM
   */
  public PersonalDetailsResponse() {
    super();
    // TODO Auto-generated constructor stub
  }

  public String getEmail() {
    return email;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


}
