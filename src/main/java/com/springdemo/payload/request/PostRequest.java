/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:57:11 PM PostRequest.java
 *         com.springdemo.payload.request SpringDemo
 */
package com.springdemo.payload.request;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 10:57:11 PM
 */
public class PostRequest {

  String text;

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  /**
   * @author Nil Panchal Jun 6, 2022 10:58:48 PM
   * @param text
   */
  public PostRequest(String text) {
    this.text = text;
  }

  /**
   * @author Nil Panchal Jun 6, 2022 10:58:50 PM
   */
  public PostRequest() {
    super();
  }


}
