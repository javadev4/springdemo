/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:02:53 PM LoginRequest.java com.springdemo.payload
 *         SpringDemo
 */
package com.springdemo.payload.request;

import javax.validation.constraints.NotBlank;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:02:53 PM
 */
public class LoginRequest {
  @NotBlank
  private String email;

  @NotBlank
  private String password;

  /**
   * @author Nil Panchal Jun 7, 2022 11:35:18 AM
   */
  public LoginRequest() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @author Nil Panchal Jun 7, 2022 11:35:15 AM
   * @param email
   * @param password
   */
  public LoginRequest(@NotBlank String email, @NotBlank String password) {
    this.email = email;
    this.password = password;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

}
