/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:13:55 PM SignUpRequest.java com.springdemo.payload
 *         SpringDemo
 */
package com.springdemo.payload.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:13:55 PM
 */
public class SignUpRequest {
  @NotBlank
  private String firstName;

  @NotBlank
  private String lastName;

  @NotBlank

  @Email
  private String email;

  @NotBlank
  private String password;


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
}
