/**
 * @author Nil Panchal Mex IT May 28, 2022 11:16:18 PM AuditingConfig.java com.springdemo.config
 *         SpringDemo
 */
package com.springdemo.config;

import java.util.Optional;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


/**
 * @author Nil Panchal Mex IT May 28, 2022 11:16:18 PM
 */
@Configuration
@EnableJpaAuditing
public class AuditingConfig {

  @Bean
  public AuditorAware<String> auditorProvider() {
    return new SpringSecurityAuditAwareImpl();
  }


}


/**
 * SpringSecurityAuditAwareImpl used to get id for updating entity
 * 
 * @author Nil Panchal Mex IT Oct 1, 2018 1:43:34 PM
 */
class SpringSecurityAuditAwareImpl implements AuditorAware<String> {

  @Override
  public Optional<String> getCurrentAuditor() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null || !authentication.isAuthenticated()
        || authentication instanceof AnonymousAuthenticationToken) {
      return Optional.empty();
    }

    UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
    return Optional.ofNullable(userPrincipal.getUsername());
  }
}
