/**
 * @author Nil Panchal Mex IT Jun 6, 2022 3:04:18 PM UserPrincipal.java com.springdemo.security
 *         SpringDemo
 */
package com.springdemo.config;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.springdemo.model.User;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 3:04:18 PM
 */
public class UserPrincipal implements UserDetails {

  /**
   * 
   */
  private static final long serialVersionUID = 8703610823104632958L;

  private String id;
  private String firstName;
  private String lastName;
  @JsonIgnore
  private String email;
  @JsonIgnore
  private String password;
  private Collection<? extends GrantedAuthority> authorities;
  private User currentUser;
  private String token;
  
  /**
   * @author Nil Panchal Jun 6, 2022 3:05:32 PM
   * @param id
   * @param name
   * @param username
   * @param email
   * @param password
   * @param authorities
   */
  public UserPrincipal(String id, String firstName, String lastName, String email, String password,
      Collection<? extends GrantedAuthority> authorities) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.authorities = authorities;
  }



  public static UserPrincipal create(User user) {
    return new UserPrincipal(user);
  }

  public UserPrincipal(User user) {
    this.id = user.getShortId();
    this.firstName = user.getFirstName();
    this.lastName = user.getLastName();
    this.email = user.getEmail();
    this.password = user.getPassword();
    this.currentUser = user;
  }



  /**
   * @author Nil Panchal Jun 6, 2022 3:05:30 PM
   */
  public UserPrincipal() {
    super();
    // TODO Auto-generated constructor stub
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    // TODO Auto-generated method stub
    return authorities;
  }

  @Override
  public String getPassword() {
    // TODO Auto-generated method stub
    return password;
  }

  @Override
  public String getUsername() {
    // TODO Auto-generated method stub
    return email;
  }

  @Override
  public boolean isAccountNonExpired() {
    // TODO Auto-generated method stub
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    // TODO Auto-generated method stub
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    // TODO Auto-generated method stub
    return true;
  }

  @Override
  public boolean isEnabled() {
    // TODO Auto-generated method stub
    return true;
  }



  public String getId() {
    return id;
  }

  public String getEmail() {
    return email;
  }



  public void setId(String id) {
    this.id = id;
  }



  public void setEmail(String email) {
    this.email = email;
  }



  public void setPassword(String password) {
    this.password = password;
  }

  public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
    this.authorities = authorities;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public User getCurrentUser() {
    return currentUser;
  }

  public void setCurrentUser(User currentUser) {
    this.currentUser = currentUser;
  }



  public String getToken() {
    return token;
  }



  public void setToken(String token) {
    this.token = token;
  }

  
}
