package com.springdemo;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.nio.charset.Charset;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.springdemo.payload.request.LoginRequest;


@SpringBootTest(classes = SpringDemoApplication.class)
@WebAppConfiguration
@TestInstance(Lifecycle.PER_CLASS)
class SpringDemoApplicationTests {
  protected MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
      MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
  protected MockMvc mockMvc;
  protected ObjectMapper mapper;
  @Autowired
  protected WebApplicationContext webApplicationContext;

  @BeforeAll
  public void setup() throws Exception {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext)
        .apply(springSecurity()).build();
    this.mapper = new ObjectMapper();
  }

  @Test
  void testLoginSuccess() throws JsonProcessingException, Exception {
    LoginRequest loginRequest = new LoginRequest();
    loginRequest.setEmail("n.panchal@gmail.com");
    loginRequest.setPassword("2207");
    ResultActions result =
        mockMvc.perform(post("/api/auth/signin").header("Origin", "http://localhost:9550")
            .content(mapper.writeValueAsString(loginRequest)).contentType(contentType));
    //result.andDo(print());
    result.andExpect(status().isOk()).andExpect(jsonPath("$.hasResult", is(true)))
        .andExpect(jsonPath("$.status", is("success")));
  }
  
  @Test
  void testLoginFail() throws JsonProcessingException, Exception {
    LoginRequest loginRequest = new LoginRequest();
    loginRequest.setEmail("n.panchal@gmail.com");
    loginRequest.setPassword("test");
    ResultActions result =
        mockMvc.perform(post("/api/auth/signin").header("Origin", "http://localhost:9550")
            .content(mapper.writeValueAsString(loginRequest)).contentType(contentType));
    //result.andDo(print());
    result.andExpect(status().isBadRequest()).andExpect(jsonPath("$.status", is("fail")));
  }
}
